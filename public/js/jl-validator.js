function showValidator(validators,form) {
  var $inputs = $('#'+form+' .form-control');
   // An array of just the ids...
   var ids = {};
   $inputs.each(function (index)
   {
       // For debugging purposes...
       var ids = $(this).attr('id');
       $("#"+ids).removeClass('is-invalid');
       // console.log(ids);
       // ids[$(this).attr('name')] = $(this).attr('id');
   });

  $.each( validators, function( key, value ) {
      $("#err_"+key).text(value[0]);
      $("#"+key).addClass('is-invalid');
      // console.log(value[0].trim());
  });
  // $("#"+form).addClass('was-validated');
}

function remove_all_validation(){
   $("form.needs-validation :input").removeClass('is-invalid');
}

$(document).ready(function(){
  $("form.needs-validation :input").on('input', function(){
      var _this = $(this);
      var checkClass = _this.hasClass('is-invalid');
      if (checkClass) {
        _this.removeClass('is-invalid');
      }
      // alert();
  });
});