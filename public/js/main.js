// const main_path = '/user4/fcrm/public';
const main_path = '';

const loader = url_path('/img/loading.gif');


function user_guide(){
  $("#user_guide_form").modal('show');
}

function url_path(path){
	var url = window.location.href;
	var arr = url.split("/");
	var result = arr[0]+"//"+arr[2];
	return result + main_path + path;
}

function num_only(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 45 && charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

  function show_img_qr(_this){
      $("#img_qr_prev").attr('src', _this.attr('src'));
      $("#qr_modal").modal('show');
  }

function loading(){
    swal({
       title: "loading",
       text: "Please wait...",
       type: "",
       timer: 1000,
       showCancelButton: false,
       showConfirmButton: false
     });
}

 $(document).ready(function(){
    $('.is_url').on('click', function(){
      var _this = $(this);

      if (_this.val()  == '' || _this.val()  == null) {
        _this.val('https://');
      }else{
        _this.val(_this.val());
      }

    });

     $('.is_url').on('focus', function(){
      var _this = $(this);

      if (_this.val()  == '' || _this.val()  == null) {
        _this.val('https://');
      }else{
        _this.val(_this.val());
      }

    });

     $('.is_url').on('blur', function(){
      var _this = $(this);

      if (_this.val()  == 'https://') {
        _this.val('');
      }else{
        _this.val(_this.val());
      }

    });

      // Add minus icon for collapse element which is open by default
      $(".collapse.show").each(function(){
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
      });

      // Toggle plus minus icon on show hide of collapse element
      $(".collapse").on('show.bs.collapse', function(){
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
      }).on('hide.bs.collapse', function(){
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
      });
  });


  $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
// Sub menu

$('.dropdown-menu a.dropdown-toggles').on('click', function(e) {
  if (!$(this).next().hasClass('show')) {
    $(this).parents('.dropdown-menu').first().find('.show').removeClass('show');
  }
  var $subMenu = $(this).next('.dropdown-menu');
  $subMenu.toggleClass('show');


  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
    $('.dropdown-submenu .show').removeClass('show');
  });


  return false;
});
