<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    protected $table = 'measurements';
    public $timestamps = false;

    protected $fillable = [
       'timestamp', 'deviceid', 'temperature', 'humidity', 'co2', 'lat', 'lng', 'pressure','open_flg','created','modified'
    ];

}