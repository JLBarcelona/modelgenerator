<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FunctionCreator extends Model
{
    protected $primaryKey = 'function_id';

    protected $table = 'gen_function';
    
    protected $fillable = [
       'function_name', 'controller', 'model', 'prefix', 'primary_id'
    ];

     public function getCreatedAtAttribute($value)
    {
        // return ucfirst($value);
        // return  \Carbon\Carbon::parse($value)->format('m-d-y h:i:s A');
        return  \Carbon\Carbon::parse($value)->format('jS F Y');
    }
}
