<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fields extends Model
{

	protected $timestamps = false;

	protected $primaryKey = 'fields_id';
    protected $table = 'gen_fields';
    
    protected $fillable = [
       'function_id', 'fields_name', 'class_name', 'type', 'row'
    ];
    
}
