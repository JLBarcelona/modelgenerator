<?php

namespace App\Http\Controllers\Creator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\FunctionCreator;
use Validator;

class FunctionCreatorController extends Controller
{

	function list_gen(){
		$gen = FunctionCreator::whereNull('deleted_at')->get();
		return response()->json(['status' => true, 'data' => $gen]);
	}

	function add_gen(Request $request){
		$function_id= $request->get('function_id');
		$function_name = $request->get('function_name');
		// $controller = $request->get('controller');
		$model = $request->get('model');
		$prefix = $request->get('prefix');
		$primary_id = $request->get('primary_id');

		$name = $request->get('name');
		$class = $request->get('class');
		$type = $request->get('type');
		$row = $request->get('row');


		$models = $this->model_generator($name, $model);
		$controllers = $this->controller_generator($model, $prefix, $primary_id);

		$validator = Validator::make($request->all(), [
			'function_name' => 'required',
			'model' => 'required',
			'prefix' => 'required',
			'primary_id' => 'required',
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{



			return response()->json(['status' => true, 'model' => $models, 'controller' => $controllers]);

			// if (!empty($function_id)) {
			// 	$gen = FunctionCreator::find($function_id);
			// 	$gen->function_name = $function_name;
			// 	$gen->controller = $controller;
			// 	$gen->model = $model;
			// 	$gen->prefix = $prefix;
			// 	$gen->primary_id = $primary_id;
			// 	if($gen->save()){
			// 		return response()->json(['status' => true, 'message' => 'Gen updated successfully!']);
			// 	}
			// }else{
			// 	$gen = new FunctionCreator;
			// 	$gen->function_name = $function_name;
			// 	$gen->controller = $controller;
			// 	$gen->model = $model;
			// 	$gen->prefix = $prefix;
			// 	$gen->primary_id = $primary_id;
			// 	if($gen->save()){
			// 		return response()->json(['status' => true, 'message' => 'Gen saved successfully!']);
			// 	}
			// }
		}
	}


	function get_model(){

	}


	function controller_generator($model,$prefix, $id){
		$output = '';

		$output .= '<?php';
		$output .= $this->line(1).'App::uses(\'AppController\', \'Controller\');';
		$output .= $this->line(1).'App::uses(\'TableRegistry\', \'ORM\')';
		$output .= $this->line(1).'App::uses(\'CakeTime\',\'Utility\');';


		$output .= $this->line(3).'class '.ucfirst($model.'Controller').' extends AppController {'.$this->line(2);

		$output.= $this->tab(1).'var $uses = array(\''.ucfirst($model).'\');';

		$output.= $this->line(2);

		$output .= $this->get_index($model, $prefix);

		$output.= $this->line(2);

		$output .= $this->get_add($model, $prefix);

		$output.= $this->line(2);

		$output .= $this->get_edit($model,$prefix,$id);

		$output.= $this->line(2);

		$output .= $this->get_delete($model,$prefix,$id);

		$output .= $this->line(2).'}';


		return $output;
	}


	function get_index($model,$prefix){
		$output = '';
		$output .= $this->tab(1).'public function index(){'.$this->line(1);
		$output .= $this->tab(2).'$this->set(\''.$prefix.'\', $this->'.ucfirst($model).'->find(\'all\'));'.$this->line(1);
		$output .= $this->tab(1).'}';

		return $output;
	}


	function get_add($model,$prefix){
		$output = '';
		$output .= $this->tab(1).'public function add(){'.$this->line(1);
		$output .= $this->tab(2).'if ($this->request->is(\'post\')){'.$this->line(1);
			$output .= $this->tab(3).'if ($this->'.ucfirst($model).'->save($this->request->data)){'.$this->line(1);
			$output .= $this->tab(4).'// $this->Flash->success(__(\'The '.$prefix.' has been saved\'))'.$this->line(1);
			$output .= $this->tab(4).'// return $this->redirect(array(\'action\' =>\'index\'))'.$this->line(1);
			$output .= $this->tab(3).'}'.$this->line(1);
		$output .= $this->tab(2).'}else{'.$this->line(1);
			$output .= $this->tab(4).'// $this->Flash->error(__(\'The '.$prefix.' could not be saved. Please, try again.\'))'.$this->line(1);
		$output .= $this->tab(2).'}'.$this->line(1);
		$output .= $this->tab(1).'}'.$this->line(1);

		return $output;
	}

	function get_edit($model,$prefix,$id){
		$output = '';
		$output .= $this->tab(1).'public function edit($id){'.$this->line(1);
		$output .= $this->tab(2).'$this->'.ucfirst($model).'->'.$id.' = $id;'.$this->line(1);
		$output .= $this->tab(2).'if ($this->request->is(array(\'post\',\'put\'))){'.$this->line(1);
			$output .= $this->tab(3).'if ($this->'.ucfirst($model).'->save($this->request->data)){'.$this->line(1);
			$output .= $this->tab(4).'// $this->Flash->success(__(\'The '.$prefix.' has been edited\'))'.$this->line(1);
			$output .= $this->tab(4).'// return $this->redirect(array(\'action\' =>\'index\'))'.$this->line(1);
			$output .= $this->tab(3).'}'.$this->line(1);
		$output .= $this->tab(2).'}else{'.$this->line(1);
			$output .= $this->tab(4).'// $this->Flash->error(__(\'The '.$prefix.' could not be edited. Please, try again.\'))'.$this->line(1);
		$output .= $this->tab(2).'}'.$this->line(1);
		$output .= $this->tab(1).'}'.$this->line(1);

		return $output;
	}

	function get_delete($model,$prefix,$id){
		$output = '';
		$output .= $this->tab(1).'public function delete($id){'.$this->line(1);
		$output .= $this->tab(2).'$this->'.ucfirst($model).'->'.$id.' = $id;'.$this->line(1);
		$output .= $this->tab(2).'if ($this->request->is(array(\'post\',\'delete\'))){'.$this->line(1);
			$output .= $this->tab(3).'if ($this->'.ucfirst($model).'->delete()){'.$this->line(1);
			$output .= $this->tab(4).'// $this->Flash->success(__(\'The '.$prefix.' has been deleted\'))'.$this->line(1);
			$output .= $this->tab(4).'// return $this->redirect(array(\'action\' =>\'index\'))'.$this->line(1);
			$output .= $this->tab(3).'}'.$this->line(1);
		$output .= $this->tab(2).'}else{'.$this->line(1);
			$output .= $this->tab(4).'// $this->Flash->error(__(\'The '.$prefix.' could not be deleted. Please, try again.\'))'.$this->line(1);
		$output .= $this->tab(2).'}'.$this->line(1);
		$output .= $this->tab(1).'}'.$this->line(1);

		return $output;
	}


	function get_html(){
		
	}

	function model_generator($data, $model){
		$output = "";

		$output .= '<?php';
		$output .= $this->line(1).'App::uses(\'AppModel\', \'Model\');';
		$output .= $this->line(3).'class '.ucfirst($model).' extends AppModel {'.$this->line(2);

		$output .= $this->tab(1).'public $validate = array(';
			foreach ($data as $val) {
				$output .= $this->line(1).$this->tab(2).'\''.$val.'\' => array('.$this->line(1);
				$output .= $this->tab(3).'\'rule\' => \'alphaNumeric\','.$this->line(1);
				$output .= $this->tab(3).'\'required\' => true'.$this->line(1);
				$output .= $this->line(1).$this->tab(2).'),';
				// if(end($data) == $val) {
			 //        $output .= '';
			 //    }else{
			 //        $output .= ',';
			 //    }
			}
		$output .=  $this->line(1).$this->tab(1).');';

		$output .= $this->line(2).'}';
		return $output;
	}

	function line($num){
		$output = '';
		for ($i=0; $i < $num; $i++) { 
			$output .= "\n";
		}

		return $output;
	}

	function tab($num){
		$output = '';
		for ($i=0; $i < $num; $i++) { 
			$output .= "\t";
		}

		return $output;
	}


	function delete_gen($function_id){
		$gen = FunctionCreator::find($function_id);
		if($gen->delete()){
			return response()->json(['status' => true, 'message' => 'Gen deleted successfully!']);
		}
	}
}
