<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Measurement;

use Carbon\Carbon;
use Faker\Generator as Faker;

class MeasurementsController extends Controller
{
    function list_mess(){
		$mess = Measurement::orderBy('id','asc')->count();
		return response()->json(['status' => true, 'data' => number_format($mess)]);
		// return 1;

	}

	function add_mess(Faker $faker, Request $request){
		ini_set('max_execution_time', 580); //5 minutes
		$data = array();
		$counter = 0;
		$validator = Validator::make($request->all(), [
			'start_date' => 'required',
			'interval' => 'required',
			'qty' => 'required',
		]);

		$interv = 30;
		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			for ($i=0; $i < $request->get('qty'); $i++) { 
				$interv = ($interv + $request->get('interval'));
				$data[] = $this->create_record($faker, $request->get('start_date'),  $interv);
				$counter++;	
			}

		
		}

		if (Measurement::insert($data)) {
			$message = $counter.' Records saved successfully!';
			return response()->json(['status' => true, 'message' => $message]);
	
		}
	}


	function delete_mess($id){
		$mess = Measurement::find($id);
		if($mess->delete()){
			return response()->json(['status' => true, 'message' => 'Mess deleted successfully!']);
		}
	}


	function create_record($faker, $start_date, $interval){
			// $mess = new Measurement;
			$array = array('timestamp' => $this->get_timestamp($start_date, $interval),
							'deviceid' => $this->last_device_id(),
							'temperature' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 50),
							'humidity' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 50),
							'co2' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 50),
							'lat' => $faker->latitude($min = -20, $max = 45),
							'lng' => $faker->longitude($min = -122, $max = 153),
							'pressure' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 50),
							'open_flg' => 1,
							'created' => now(),
							'modified' => now());
			return $array;
	}


	function get_timestamp($start_date, $interval){
		$latest = Measurement::orderBy('id', 'desc')->first();
		if (!empty($latest->timestamp)) {
			return $newDateTime = Carbon::createFromFormat('Y-m-d H:i:s', $latest->timestamp)->addSecond($interval);
		}else{
			return $newDateTime = Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s', strtotime($start_date.' 01:00:00')))->addSecond($interval);
		}
	}

	function last_device_id(){
		$latest = Measurement::orderBy('id', 'desc')->first();
		$param = (!empty($latest->deviceid))? ($latest->deviceid + 1) : 1;
		if ($param > 100) {
			return 1;
		}else{
			return $param;
		}
	}

}
