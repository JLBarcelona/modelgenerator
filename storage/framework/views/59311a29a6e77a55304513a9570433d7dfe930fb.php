<!DOCTYPE html>
<html>
	<?php echo $__env->make('Layout.header', ['type' => 'admin', 'title' => 'Create Function', 'icon' => asset('img/logo.png') ], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<body class="font-base">
	<?php echo $__env->make('Layout.nav', ['type' => 'admin'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
	<div class="container-fluid">
		<div class="row pt-3">
			<div class="col-sm-12">
				<form class="needs-validation" id="function_creator_form" action="<?php echo e(url('/gen/add_gen')); ?>" novalidate>
				<div class="card">
					<div class="card-header bg-primary text-white font-base-xl bold">Create Cakphp Crud</div>
					<div class="card-body">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-row">
										<input type="hidden" id="function_id" name="function_id" placeholder="" class="form-control" required>
									<div class="col-sm-12">
											<span class="h4">Form Details</span>
											<hr>
										</div>
									<div class="form-group col-sm-12">
										<label>Function Name </label>
										<input type="text" id="function_name" value="property" name="function_name" placeholder="Example: Property Functions" class="form-control " required>
										<div class="invalid-feedback" id="err_function_name"></div>
									</div>
									<!-- <div class="form-group col-sm-12">
										<label>Controller </label>
										<input type="text" id="controller" value="property" name="controller" placeholder="ControllerName" class="form-control " required>
										<div class="invalid-feedback" id="err_controller"></div>
									</div> -->
									<div class="form-group col-sm-12">
										<label>Model </label>
										<input type="text" id="model" value="property" name="model" placeholder="Model Name" class="form-control " required>
										<div class="invalid-feedback" id="err_model"></div>
									</div>
									<div class="form-group col-sm-12">
										<label>Prefix </label>
										<input type="text" id="prefix" value="property" name="prefix" placeholder="example: user = $user" class="form-control " required>
										<div class="invalid-feedback" id="err_prefix"></div>
									</div>
									<div class="form-group col-sm-12">
										<label>Primary Id </label>
										<input type="text" id="primary_id" value="property" name="primary_id" placeholder="Tables primary key Example user_id" class="form-control " required>
										<div class="invalid-feedback" id="err_primary_id"></div>
									</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="row">
										<div class="col-sm-12">
											<span class="h4">Fields</span>
											<button type="button" class="btn float-right btn-sm btn-success" onclick="add_fields();"><i class="fa fa-plus"></i></button>
											<hr>
										</div>

										<div class="col-sm-12">
											<input type="hidden" value="0" id="row_count">
											<div id="field_content"></div>
										</div>
									</div>
								</div>
							</div>
					
					</div>
					<div class="card-footer text-right">
						<button class="btn btn-primary btn-sm" type="submit">Save & Generate</button>
					</div>
				</div>
			 </form>
			</div>
		</div>
	</div>
	
<div class="modal fade" role="dialog" id="result_modal">
  <div class="modal-dialog model-lg" style="max-width: 95% !important;">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">
        Result
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
        	<!-- <div class="col-sm-12">
        		<h3>Form</h3>
        		<div class="form-group">
        			<textarea class="form-control bg-dark text-white" readonly="" id="form_code"></textarea>
        		</div>
        	</div> -->
        	<div class="col-sm-12">
        		<h3>Model</h3>
        		<div class="form-group">
        			<textarea class="form-control bg-dark text-white" rows="15" readonly="" id="model_code" style="font-size:14px;"></textarea>
        		</div>
        	</div>
        	<div class="col-sm-12">
        		<h3>Controller</h3>
        		<div class="form-group">
        			<textarea class="form-control bg-dark text-white" rows="15" readonly="" id="controller_code" style="font-size:14px;"></textarea>
        		</div>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>

</body>
	<?php echo $__env->make('Layout.footer', ['type' => 'admin'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</html>

<script type="text/javascript">

	function add_fields(){
		let row_count = $("#row_count").val();
		let content = $("#field_content");

		let row_counter = (Number(row_count)+1);
		let data = `<div class="row mb-2 animated slideInDown" id="row_`+row_counter+`">
						<div class="col-sm-3"><input type="text" class="form-control" placeholder="Field name" name="name[]" id="name_`+row_counter+`"></div>
						<div class="col-sm-3">
							<select name="type[]" id="name_`+row_counter+`" class="form-control">
								<option value="text">Text</option>
								<option value="password">Password</option>
								<option value="date">Date</option>
								<option value="file">File</option>
								<option value="number">Number</option>
								<option value="email">Email</option>
								<option value="select">Select</option>
							</select>
						</div>
						<div class="col-sm-3"><input type="text" class="form-control" placeholder="Additional class" name="class[]" id="name_`+row_counter+`"></div>
						<div class="col-sm-2"><input type="text" class="form-control" placeholder="column" name="row[]" id="name_`+row_counter+`"></div>
						<div class="col-sm-1 text-right">
							<button class="btn btn-danger btn-sm" onclick="delete_content(this)" data-row="`+row_counter+`" type="button"><i class="fa fa-trash"></i></button>
						</div>
					</div>`;

		content.append(data);
		$("#row_count").val(row_counter);
	
	}

	function delete_content(_this){
		let row_count = $("#row_count").val();
		let my_count =  $(_this).attr('data-row');
		// let content = ("#row_"+my_count);
		$("#row_"+my_count).slideUp('fast', () => {
			$("#row_"+my_count).remove();
		});
		// alert();
		$("#row_count").val((Number(row_count)-1));
	}

	$("#function_creator_form").on('submit', function(e){
		var url = $(this).attr('action');
		// alert(url);
		var mydata = $(this).serialize();
		// console.log(mydata);
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
					$("#result_modal").modal({'backdrop':'static'});
					// $("#form_code").val(response.controller);
					$("#model_code").val(response.model);
					$("#controller_code").val(response.controller);
					console.log(response);
				if(response.status == true){
					// console.log(response)
					
					showValidator(response.error,'function_creator_form');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'function_creator_form');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

</script>


<!-- Javascript Function-->
<script>
	var tbl_gen;
	function show_gen(){
		if (tbl_gen) {
			tbl_gen.destroy();
		}
		var url = main_path + '/gen/list_gen';
		tbl_gen = $('#tbl_gen').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "function_name",
		"title": "Function_name",
	},{
		className: '',
		"data": "controller",
		"title": "Controller",
	},{
		className: '',
		"data": "model",
		"title": "Model",
	},{
		className: '',
		"data": "prefix",
		"title": "Prefix",
	},{
		className: '',
		"data": "primary_id",
		"title": "Primary_id",
	},{
		className: 'width-option-1 text-center',
		"data": "function_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_gen(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
				newdata += '<button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_gen(this)" type="button"><i class="fa fa-edit"></i> delete</button>';
				return newdata;
			}
		}
	]
	});
	}

	function delete_gen(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/gen/delete_gen/' + data.function_id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this gen?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_gen(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#function_id').val(data.function_id);
		$('#function_name').val(data.function_name);
		$('#controller').val(data.controller);
		$('#model').val(data.model);
		$('#prefix').val(data.prefix);
		$('#primary_id').val(data.primary_id);
	}
</script>

<?php /**PATH C:\xampp\htdocs\BNJ\modelgenerator\resources\views/Creator/create.blade.php ENDPATH**/ ?>