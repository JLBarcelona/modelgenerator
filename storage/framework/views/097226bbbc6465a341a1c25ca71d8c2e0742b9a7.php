<!-- Common -->
<script type="text/javascript" src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.4.4/umd/popper.min.js"></script>
<script type="text/javascript" src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/ajaxsetup.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/main.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/jl-validator.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('js/sweetalert.min.js')); ?>"></script>
<?php if($type == 'home'): ?>

<?php elseif($type == 'admin'): ?>
<script type="text/javascript" src="<?php echo e(asset('datatables/jquery.dataTables.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('datatables/rowdata.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('datatables/rowresponsive.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('datatables/dataTables.bootstrap4.min.js')); ?>"></script>
<?php endif; ?>


<script type="text/javascript">
$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
    if (!$(this).next().hasClass('show')) {
        $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
    }
    var $subMenu = $(this).next(".dropdown-menu");
    $subMenu.toggleClass('show');
    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
        $('.dropdown-submenu .show').removeClass("show");
    });
    return false;
});
</script><?php /**PATH C:\xampp\htdocs\BNJ\modelgenerator\resources\views/Layout/footer.blade.php ENDPATH**/ ?>