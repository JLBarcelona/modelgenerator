<?php if($type == 'home'): ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand bold" href="#">Function Generator</a>
</nav>
<?php elseif($type == 'admin'): ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="nav-brand bold nav-link  text-info" href="<?php echo e(url('/')); ?>"><i class="fa fa-code"></i> <span style="color: orange;">CODY</span>GENERATOR</a>

   <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link pt-2" href="<?php echo e(url('/create')); ?>">Function Creator</a>
        </li>
      <!--   <li class="nav-item">
          <a class="nav-link" href="#">Link</a>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#">Disabled</a>
        </li> -->
    </ul>

</nav>
<?php endif; ?>
<?php /**PATH C:\xampp\htdocs\BNJ\modelgenerator\resources\views/Layout/nav.blade.php ENDPATH**/ ?>