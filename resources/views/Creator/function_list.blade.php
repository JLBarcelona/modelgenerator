<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'Function List', 'icon' => asset('img/logo.png') ])
<body class="font-base">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
<!-- 		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				
				<div class="card">
					<div class="card-header h5"><span id="record_count">0</span></div>
					<div class="card-body">
						<form class="needs-validation" id="mess_form" action="{{ url('mess/add_mess') }}" novalidate>
							<div class="form-row">
							<input type="hidden" id="id" name="id" placeholder="" class="form-control" required>
							<div class="form-group col-sm-6">
								<label>Start Date </label>
								<input type="date" id="start_date" name="start_date" placeholder="" class="form-control " value="2021-01-01" required>
								<div class="invalid-feedback" id="err_start_date"></div>
							</div>
							<div class="form-group col-sm-6">
								<label>Interval (Seconds)</label>
								<input type="number" id="interval" name="interval" placeholder="" class="form-control" value="30" required>
								<div class="invalid-feedback" id="err_interval"></div>
							</div>
							<div class="form-group col-sm-12">
								<label>How many records you want to generate ? </label>
								<input type="number" id="qty" name="qty" placeholder="" class="form-control " required>
								<div class="invalid-feedback" id="err_qty"></div>
							</div>
							
							<div class="col-sm-12 text-right">
								<button class="btn btn-dark btn-sm" type="submit">Submit</button>
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer"></div>
			</div>
		</div>
	</div> -->
</div>

<audio id="success">
<source src="{{ asset('sound/success.mp3') }}" type="audio/mpeg">
</audio>

<audio id="error">
<source src="{{ asset('sound/error.mp3') }}" type="audio/mpeg">
</audio>
</body>
	@include('Layout.footer', ['type' => 'admin'])
</html>

<!-- Javascript Function-->
<script>
	 var sound; 

	 function get_latest(){
		var url = main_path + '/mess/list_mess';
	 	$.ajax({
	 	    type:"GET",
	 	    url:url,
	 	    data:{},
	 	    dataType:'json',
	 	    beforeSend:function(){
	 	    },
	 	    success:function(response){
	 	      // console.log(response);
	 	      $("#record_count").text(response.data);
	 	    },
	 	    error: function(error){
	 	      // console.log(error);
	 	    }
	 	  });
	 }

	 setInterval(function(){
	 	get_latest();
	 },500)

	$("#mess_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);
		let qty = $("#qty").val();
		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
				swal("Please Wait..", "Inserting "+qty+" data","info");
			},
			success:function(response){
					console.log(response);
				if(response.status == true){
					// console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'mess_form');
		            sound = document.getElementById("success");
		           	playAudio();
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'mess_form');
				}
			
			},
			error:function(error){
				sound = document.getElementById("error");
				swal("Success", "Please Advice JL for this error.", "error");
				playAudio();
				console.log(error);
			}
		});
		
	});

	function playAudio() { 
	    sound.play(); 
	  } 

	function delete_mess(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/mess/delete_mess/' + data.id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this mess?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function edit_mess(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		$('#id').val(data.id);
		$('#timestamp').val(data.timestamp);
		$('#deviceid').val(data.deviceid);
		$('#temperature').val(data.temperature);
		$('#humidity').val(data.humidity);
		$('#co2').val(data.co2);
		$('#lat').val(data.lat);
		$('#lng').val(data.lng);
		$('#pressure').val(data.pressure);
		$('#open_flag').val(data.open_flag);
		$('#created').val(data.created);
		$('#modified').val(data.modified);
	}
</script>