<div class="modal fade campaign-update" id="campaign-update" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Update Campaign
                </h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <form id="campaign-form" method="post" action="{{ route('campaign.edit.save') }}">
                @csrf
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="campaign_id" name="campaign_id" class="form-control">
                    <div class="col-lg-12">
                        <a class="btn btn-success waves-effect float-left email_forward btn-right" href="javascript:void(0);" id="email_forward">Send Email</a>
                        <a class="btn btn-success waves-effect float-left email_test_forward btn-right" href="javascript:void(0);" id="email_test_forward">Send Test Email</a>
                        <a class="btn btn-success waves-effect float-left email_report" href="javascript:void(0);" id="email">Report</a>
                        <button class="btn btn-success waves-effect float-right" type="submit" name="save" id="save">Save
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
