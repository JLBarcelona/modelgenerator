<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::namespace('Creator')->group(function () {
	Route::get('/', function () {
	    return view('Creator.function_list');
	});

	Route::get('/create/', function () {
	    return view('Creator.create');
	});

 Route::get('/gen/list_gen', 'FunctionCreatorController@list_gen')->name('gen.list');
 Route::post('/gen/add_gen', 'FunctionCreatorController@add_gen')->name('gen.add');
 Route::get('/gen/delete_gen/{function_id}', 'FunctionCreatorController@delete_gen')->name('gen.delete');

});


 Route::get('/mess/list_mess', 'MeasurementsController@list_mess')->name('mess.list');
 Route::post('/mess/add_mess', 'MeasurementsController@add_mess')->name('mess.add');
 Route::get('/mess/delete_mess/{id}', 'MeasurementsController@delete_mess')->name('mess.delete');